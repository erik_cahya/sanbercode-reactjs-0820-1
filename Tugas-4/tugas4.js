// Soal 1
var firstLoop = 2;

console.log('LOOPING PERTAMA');
while (firstLoop <= 20) {
    console.log(firstLoop + ' ' + '-' + ' ' + 'I Love Coding');
    firstLoop += 2;
}
console.log('\n');
var secondLoop = 20;

console.log('LOOPING KEDUA');
while (secondLoop >= 1) {
    console.log(secondLoop + ' ' + '-' + ' ' + 'I Love Coding');
    secondLoop -= 2;
}
console.log('\n');


// soal 2
var thirdLoop = 1;

while (thirdLoop <= 20) {
    if (thirdLoop % 3 === 0 && thirdLoop % 2 === 1) {
        console.log(thirdLoop + ' ' + 'I Love Coding');
    }
    else if (thirdLoop % 2 === 1) {
        console.log(thirdLoop + ' ' + 'Santai');
    }
    else if (thirdLoop % 2 === 0) {
        console.log(thirdLoop + ' ' + 'Berkualitas');
    }
    thirdLoop++;
}
console.log('\n');


// Soal 3
var s = '';
for (var baris = 0; baris <= 7; baris++) {

    for (var kolom = 0; kolom < baris; kolom++) {
        s = s + '#';
    }
    s = s + '\n';
}
console.log(s);


// Soal 4
var kalimat = "saya sangat senang belajar javascript";
var kalimatArray = kalimat.split(" ");
console.log(kalimatArray);
console.log('\n');


// Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var arrayDaftarBuah = daftarBuah.sort().join("\n");
console.log(arrayDaftarBuah);