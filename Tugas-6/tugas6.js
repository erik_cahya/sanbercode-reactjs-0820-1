// Soal 1
const phi = 3.14;
let luasLingkaran = r => phi * r * r;
let kelilingLingkaran = r => 2 * phi * r;

console.log(luasLingkaran(5));
console.log(kelilingLingkaran(5));



// Soal 2
function addKata() {
    const firstWord = "saya"
    const secondWord = "adalah"
    const thirdWord = "seorang"
    const fourthWord = "frontend"
    const fifthWord = "developer"

    let kalimat = `${firstWord}\n${secondWord}\n${thirdWord}\n${fourthWord}\n${fifthWord}`;
    return kalimat;
}
console.log(addKata());



// Soal 3
const newFunction = (firstName, lastName) => {
    let fullName = function () {
        console.log(firstName + " " + lastName)
        return
    }
    return { firstName, lastName, fullName };
}
// //Driver Code 
newFunction("William", "Imoh").fullName()




// Soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation);




// Soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east];
//Driver Code
console.log(combined);
