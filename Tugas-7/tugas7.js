// Soal 1

class Animal {
    constructor(nama) {
        this._animalName = nama
        this.legs = 4
        this.cold_blooded = false;
    }

    get name() {
        return this._animalName
    }

    set name(x) {
        this._animalName = x
    }
}

class Ape extends Animal {
    constructor(nama) {
        super(nama)
        this.legs = 2;
    }

    yell() {
        return "Auooo"
    }

}

class Frog extends Animal {
    constructor(nama) {
        super(nama)
    }

    jump() {
        return "hop hop"
    }

}

let sheep = new Animal("shaun");
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

console.log("\n");

let sungokong = new Ape("Kera Sakti");
console.log(sungokong.yell());

console.log("\n");

let kodok = new Frog("buduk");
console.log(kodok.jump());





// Soal 2

class Clock {
    constructor({ template }) {
        this.template = template
        this.timer;
    }
    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }
    stop() {
        clearInterval(this.timer);
    }
    start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 3000);
    }


}
var clock = new Clock({ template: 'h:m:s' });
clock.start();  
